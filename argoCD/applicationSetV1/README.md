# Generating Applications with ApplicationSet

The ApplicationSet controller is a part of Argo CD adds Application automation, and seeks to improve multi-cluster support and cluster multitenant support within Argo CD. Argo CD Applications may be templated from multiple different sources, including from Git or Argo CD's own defined cluster list.

The ApplicationSet controller is installed alongside Argo CD (within the same namespace), and the controller automatically generates Argo CD Applications based on the contents of a new ApplicationSet Custom Resource (CR).

Here is an example of an ApplicationSet resource that can be used to target an Argo CD Application to multiple clusters:

[applicationSetV1/argo-appSet.yaml]

```
apiVersion: argoproj.io/v1alpha1
kind: ApplicationSet
metadata:
  name: guestbook
spec:
  generators:
  - list:
      elements:
      - cluster: result
        url: "https://kubernetes.default.svc"
      - cluster: vote
        url: "https://kubernetes.default.svc"
      - cluster: worker
        url: "https://kubernetes.default.svc"
  template:
    metadata:
      name: '{{cluster}}-guestbook'
    spec:
      project: default
      source:
        repoURL: https://gitlab.com/F4talny/example-voting-app.git
        targetRevision: HEAD
        path: argoCD/applicationSetV1/{{cluster}}
      destination:
        server: '{{url}}'
        namespace: guestbook
      syncPolicy:
        automated:
          prune: true
          selfHeal: true
```

The List generator passes the url and cluster fields into the template as {{param}}-style parameters, which are then rendered into three corresponding Argo CD Applications (one for each defined cluster). Targeting new clusters (or removing existing clusters) is simply a matter of altering the ApplicationSet resource, and the corresponding Argo CD Applications will be automatically created.

# Installing Argo CD Helm chart

```
helm install argo-cd argo/argo-cd
```

Check if all services are up and running:

```
kubectl get svc -a
```
# Web UI 

The Helm chart doesn’t install an Ingress by default, to access the Web UI we have to port-forward to the argocd-server service:

```
kubectl port-forward svc/argo-cd-argocd-server 8080:443
```

We can then visit http://localhost:8080 to access it.

The default username is admin. The password is auto-generated and we can get it with:

```
kubectl get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```


# Create an Application manifest for each service 

[applicationSetV1/result/result.yaml]

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: result
  labels:
    app: result
spec:
  replicas: 1
  selector:
    matchLabels:
      app: result
  # project: default
  template:
    metadata:
      labels:
        app: result
    spec:
      containers:
      - name: result
        image: registry.gitlab.com/f4talny/example-voting-app/result:latest
        ports:
        - containerPort: 80
          name: result
---
apiVersion: v1
kind: Service
metadata:
  name: result
  labels:
    app: result
spec:
  type: LoadBalancer
  ports:
  - port: 5001
    targetPort: 80
    name: result-service
  selector:
    app: result
```

[applicationSetV1/vote/vote.yaml]

```
apiVersion: v1
kind: Service
metadata:
  name: vote
  labels:
    apps: vote
spec:
  type: LoadBalancer
  ports:
    - port: 5000
      targetPort: 80
      name: vote-service
  selector:
    app: vote
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: vote
  labels:
    app: vote
spec:
  replicas: 1
  selector:
    matchLabels:
      app: vote
  template:
    metadata:
      labels:
        app: vote
    spec:
      containers:
      - name: vote
        image: registry.gitlab.com/f4talny/example-voting-app/vote:latest
        ports:
        - containerPort: 80
          name: vote
```
[applicationSetV1/worker/worker.yaml]

```
apiVersion: v1
kind: Service
metadata: 
  labels: 
    apps: worker
  name: worker
spec: 
  clusterIP: None
  selector: 
    app: worker
--- 
apiVersion: apps/v1
kind: Deployment
metadata: 
  labels: 
    app: worker
  name: worker
spec: 
  replicas: 1
  selector:
    matchLabels:
      app: worker
  template: 
    metadata: 
      labels: 
        app: worker
    spec: 
      containers: 
      - image: registry.gitlab.com/f4talny/example-voting-app/worker:latest
        name: worker
```

# Before we install our ApplicationSet, we need to create namespace, in this case our applications are in guestbook namespace, to do that all we have to do is simple past this command:

``` 
kubectl create ns guestbook
```

# After that we are able to install our ApplicationSet using manifest file, we created in the beginning:

```
kubectl apply -f argo-appSet.yaml
```