# What is Argo CD?

Argo CD is a GitOps tool to automatically synchronize the cluster to the desired state defined in a Git repository. Each workload is defined declarative through a resource manifest in a YAML file. Argo CD checks if the state defined in the Git repository matches what is running on the cluster and synchronizes it if changes were detected.

For example, instead of manually running CLI commands to update Kubernetes resources with kubectl apply or helm upgrade, we would update a YAML file in our Git repository that contains an Application manifest. Argo CD periodically checks this manifest for changes and will automatically synchronize resources that are defined in it with the ones that are running on our cluster.


# Creating Helm chart

We’ll use Helm to install Argo CD with the official chart from argoproj/argo-helm. We create a Helm umbrella chart that pulls in the original Argo CD chart as a dependency.

Using this approach we have the possibility to bundle extra resources with the chart. For example, we can install credentials that are used to authenticate with private Git or Helm repositories by placing them in the chart template/ directory.

To create the umbrella chart we make a directory in our Git repository and place two files in it:

```
mkdir -p charts/argo-cd
```
[charts/argo-cd/Chart.yaml]

```
apiVersion: v2
name: argo-cd
version: 1.0.0
dependencies:
  - name: argo-cd
    version: 4.9.4
    repository: https://argoproj.github.io/argo-helm
```

[charts/argo-cd/values.yaml]

```
argo-cd:
  dex:
    enabled: false
  server:
    extraArgs:
      - --insecure
    config:
      repositories: |
        - type: helm
          name: argo-cd
          url: https://argoproj.github.io/argo-helm
```
### Override values:
-   We disable the dex component that is used for integration with external auth providers
-   We start the server with the --insecure flag to serve the Web UI over http (This is assuming we’re using a local k8s server without TLS setup)


## Before we install the chart we need to generate a Chart.lock file:

```
helm repo add argo-cd https://argoproj.github.io/argo-helm
helm dep update charts/argo-cd/
```

This will generate two files: 

- Chart.lock
- charts/argo/cd-4.9.4.tgz

The tgz file is the downloaded dependency and not required in our Git repository, we can therefore exclude it. Argo CD will download the dependencies by itself based on the Chart.lock file.

We exclude it by creating a .gitignore file in the chart directory:

```
echo "charts/" > charts/argo-cd/.gitignore
```

# Installing our Argo CD Helm chart

```
helm install argo-cd argo/argo-cd
```

Check if all services are up and running:

```
kubectl get svc -a
```
# Web UI 

The Helm chart doesn’t install an Ingress by default, to access the Web UI we have to port-forward to the argocd-server service:

```
kubectl port-forward svc/argo-cd-argocd-server 8080:443
```

We can then visit http://localhost:8080 to access it.

The default username is admin. The password is auto-generated and we can get it with:

```
kubectl get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

### In practice Argo CD applications could be added through the Web UI or CLI, but since we want to manage everything declaratively we’ll write Application manifests in YAML and put them into our Git repo.


# Creating the root app

To add an application to Argo CD we need to add an Application resource to Kubernetes. It specifies the Git repository and the file path under which to find the manifests.

The root application has one task: it generates Application manifests for other applications. Argo CD will watch the root application and synchronize any applications that it generates.

With this setup we only have to add one application manually: the root application.

We create it in an [apps/] directory and put a [Chart.yaml] file and an empty [values.yaml] file in it. In our git repo we run:

```
mkdir -p apps/templates
touch apps/values.yaml
```

[apps/Chart.yaml]

```
apiVersion: v2
name: root
version: 1.0.0
```

We create the Application manifest for our root application in [apps/templates/root.yaml.] This allows us to do any updates to the root application itself through Argo CD:

[apps/templates/root.yaml:]

```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: root
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    server: https://kubernetes.default.svc
    namespace: default
  project: default
  source:
    path: argoCD/apps/
    repoURL: https://gitlab.com/F4talny/example-voting-app.git
    targetRevision: HEAD
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

The above Application watches the Helm chart under apps/ (our root application) and synchronizes it if changes were detected.

- Note: Argo CD will not use helm install to install charts. It will render the chart with helm template and then apply the output with kubectl. This means we can’t run helm list on a local machine to get all installed releases.

Deploy our root application:

```
helm template apps/ | kubectl apply -f root.yaml
```

# Letting Argo CD manage itself

With this approach any updates to our Argo CD deployment can be made by modifying files in our Git repository rather than running manual commands.

We put the application manifest in [apps/templates/argo-cd.yaml]

```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: argo-cd
  namespace: default
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    server: https://kubernetes.default.svc
    namespace: default
  project: default
  source:
    path: argoCD/charts/argo-cd
    repoURL: https://gitlab.com/F4talny/example-voting-app.git
    targetRevision: HEAD
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

Push the file to your Git repository:

```
git add apps/templates/argo-cd.yaml
git ci -m 'add argo-cd application'
git push
```

In the Web UI we should now see the root application being OutOfSync and Syncing.

# Installing Voting App

First we create an Application manifest for each service [apps/templates/result.yaml] 

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: result
  labels:
    app: result
spec:
  replicas: 1
  selector:
    matchLabels:
      app: result
  # project: default
  template:
    metadata:
      labels:
        app: result
    spec:
      containers:
      - name: result
        image: registry.gitlab.com/f4talny/example-voting-app/result:latest
        ports:
        - containerPort: 80
          name: result
---
apiVersion: v1
kind: Service
metadata:
  name: result
  labels:
    app: result
spec:
  #type: LoadBalancer
  ports:
  - port: 5001
    targetPort: 80
    name: result-service
  selector:
    app: result
```

[apps/templates/vote.yaml] 

```
apiVersion: v1
kind: Service
metadata:
  name: vote
  labels:
    apps: vote
spec:
  #type: LoadBalancer
  ports:
    - port: 5000
      targetPort: 80
      name: vote-service
  selector:
    app: vote
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: vote
  labels:
    app: vote
spec:
  replicas: 2
  selector:
    matchLabels:
      app: vote
  template:
    metadata:
      labels:
        app: vote
    spec:
      containers:
      - name: vote
        image: https://gitlab.com/F4talny/example-voting-app/container_registry/f4talny/example-voting-app/vote:latest
        ports:
        - containerPort: 80
          name: vote
```

[apps/templates/worker.yaml] 

```
apiVersion: v1
kind: Service
metadata: 
  labels: 
    apps: worker
  name: worker
spec: 
  clusterIP: None
  selector: 
    app: worker
--- 
apiVersion: apps/v1
kind: Deployment
metadata: 
  labels: 
    app: worker
  name: worker
spec: 
  replicas: 1
  selector:
    matchLabels:
      app: worker
  template: 
    metadata: 
      labels: 
        app: worker
    spec: 
      containers: 
      - image: https://gitlab.com/F4talny/example-voting-app/container_registry/f4talny/example-voting-app/worker:latest
        name: worker
```

### To deploy the application all we have to do is push the manifest to our Git repository:

```
git add /apps/templates/*.yaml
git commit -m "add voting app"
git push
```