package worker;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import java.sql.*;
import org.json.JSONObject;

class Worker {

  private static final String DB_HOST = readEnvVar("DB_HOST", "defaultHost");
  private static final String DB_NAME = readEnvVar("DB_NAME", "defaultDbName");
  private static final String DB_USERNAME = readEnvVar("DB_USERNAME", "defaultDbUsername");
  private static final String DB_PASSWORD = readEnvVar("DB_PASSWORD", "defaultDbPassword");
  private static final String REDIS_HOST = readEnvVar("REDIS_HOST", "defaultRedisHost");

  public static void main(String[] args) {
    try {
      System.err.println("Environment variables:");
      System.err.printf("DB_HOST = %s\n", DB_HOST);
      System.err.printf("DB_NAME = %s\n", DB_NAME);
      System.err.printf("DB_USERNAME = %s\n", DB_USERNAME);
      System.err.printf("DB_PASSWORD = %s\n", DB_PASSWORD);
      System.err.printf("REDIS_HOST = %s\n\n", REDIS_HOST);

      Jedis redis = connectToRedis(REDIS_HOST);
      Connection dbConn = connectToDB(DB_HOST);

      System.err.println("Watching vote queue");

      while (true) {
        String voteJSON = redis.blpop(0, "votes").get(1);
        JSONObject voteData = new JSONObject(voteJSON);
        String voterID = voteData.getString("voter_id");
        String vote = voteData.getString("vote");

        System.err.printf("Processing vote for '%s' by '%s'\n", vote, voterID);
        updateVote(dbConn, voterID, vote);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private static String readEnvVar(String envVarName, String defaultValue) {
    String envVar = System.getenv(envVarName);
    return envVar != null ? envVar : defaultValue;
  }

  static void updateVote(Connection dbConn, String voterID, String vote) throws SQLException {
    PreparedStatement insert = dbConn.prepareStatement(
      "INSERT INTO votes (id, vote) VALUES (?, ?)");
    insert.setString(1, voterID);
    insert.setString(2, vote);

    try {
      insert.executeUpdate();
    } catch (SQLException e) {
      PreparedStatement update = dbConn.prepareStatement(
        "UPDATE votes SET vote = ? WHERE id = ?");
      update.setString(1, vote);
      update.setString(2, voterID);
      update.executeUpdate();
    }
  }

  static Jedis connectToRedis(String host) {
    Jedis conn = new Jedis(host);

    while (true) {
      try {
        conn.keys("*");
        break;
      } catch (JedisConnectionException e) {
        System.err.println("Waiting for redis");
        sleep(1000);
      }
    }

    System.err.println("Connected to redis");
    return conn;
  }

  static Connection connectToDB(String host) throws SQLException {
    Connection conn = null;

    try {

      Class.forName("org.postgresql.Driver");
      String url = "jdbc:postgresql://" + host + "/" + DB_NAME;

      while (conn == null) {
        try {
          conn = DriverManager.getConnection(url, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
          System.err.println("Waiting for db");
          sleep(1000);
        }
      }

      PreparedStatement st = conn.prepareStatement(
        "CREATE TABLE IF NOT EXISTS votes (id VARCHAR(255) NOT NULL UNIQUE, vote VARCHAR(255) NOT NULL)");
      st.executeUpdate();

    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      System.exit(1);
    }

    System.err.println("Connected to db");
    return conn;
  }

  static void sleep(long duration) {
    try {
      Thread.sleep(duration);
    } catch (InterruptedException e) {
      System.exit(1);
    }
  }
}
